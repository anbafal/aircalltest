
1 - OK
2-  OK
3 - OK
4 - OK with 4 stats

5- Improvement UX/UI:

   - A way to disconnect from menu
   - Notify the user has soon as connection lost
   - Highlight key information with appropriate color
   - For other improvements please have a look through the code by doing
   a search on "//todo".

6 - Improvement architecture:

   - Offline support
   - If apps grow, having separate kotlin library for domain, data and keep app to have both presentation and framework package.
     This will be avoid using some class of layer by mistake.
   - For other improvements please have a look through the code by doing a search on "//todo".

7 - Think that I should send the app at least thursday morning. So I know that will do a search to unit test with coroutine,
room and implement it.

--------------------------Submission-------------------------
- App architecture :

  Based on clean architecture and recommended MVVM pattern by Google.

- Choice of libraries :

  Using Jetpack libraries as they follows the best practices for Android Development and let me focus on
  features. You shall notice that I used an alpha release (like Hilt & Paging 3). For me it is a way to keep learning new things added
  to Android. Of course if it is an production app I will not play this game unless the release is too far away and during
  this laps of time, will have a stable release.
  By the way, it is by following the same philosophy to test new things for me that I use coroutine for the first time.


- Most difficult part of the challenge:

  Working on project beside continuing interviews to search a job.
  Learn new things while managing to provide a running application.
  All these two points are finally overcome.

- Testing strategy:

  My strategy will be to unit test UseCase as it is the core of app.

- Percentage of completion:

  Around 60 %. Will need around 5-7 days to complete this app with testing.