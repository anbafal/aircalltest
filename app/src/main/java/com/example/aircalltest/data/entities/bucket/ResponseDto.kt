package com.example.aircalltest.data.entities.bucket


import com.google.gson.annotations.SerializedName

data class ResponseDto(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val buckets: List<BucketDto>,
    @SerializedName("total_count")
    val totalCount: Int
)