package com.example.aircalltest.data.repository.bucket

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.domain.Stat
import com.example.aircalltest.domain.repository.IBucketRepository
import com.example.aircalltest.domain.usecase.base.StatParams
import com.example.aircalltest.domain.utils.DispatcherProvider
import com.example.aircalltest.framework.retrofit.GithubService
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class BucketRepository @Inject constructor(
    private val apiService: GithubService,
    private val dispatcher: DispatcherProvider
) :
    IBucketRepository {


    override fun getTopKotlinBuckets(): Flow<PagingData<Bucket>> {
        val bucketPagingConfig = PagingConfig(pageSize = BucketPagingSource.PAGE_SIZE)
        val bucketPagingFactory = { BucketPagingSource(apiService) }

        return Pager(config = bucketPagingConfig, pagingSourceFactory = bucketPagingFactory).flow
    }

    //todo: caching from database
    override suspend fun getStats(statParams: StatParams): Stat {
        // First get from network
        return withContext(dispatcher.io()) {
            val allIssuesDeferred = async { allIssues(statParams) }
            val allPullsDeferred = async { allPulls(statParams) }
            val allBugsDeferred = async { allBugs(statParams) }
            val allCommitsDeferred = async { allCommits(statParams) }

            Stat(
                allIssuesDeferred.await(),
                allPullsDeferred.await(),
                allBugsDeferred.await(),
                allCommitsDeferred.await()
            )
        }
    }


    private suspend fun allIssues(statParams: StatParams): String {

        val query =
            "${defaultQueryIssues(statParams.bucketName)}+created:${epochToEnglishDate(statParams.startDate)}..${epochToEnglishDate(
                statParams.endDate
            )}"
        return apiService.getIssuesByBucket(query).await().totalCount.toString()
    }

    private suspend fun allPulls(statParams: StatParams): String {
        val query =
            "${defaultQueryIssues(statParams.bucketName)}+type:pr+created:${epochToEnglishDate(
                statParams.startDate
            )}..${epochToEnglishDate(
                statParams.endDate
            )}"
        return apiService.getIssuesByBucket(query).await().totalCount.toString()
    }

    private suspend fun allBugs(statParams: StatParams): String {
        val query =
            "${defaultQueryIssues(statParams.bucketName)}+label:bug+created:${epochToEnglishDate(
                statParams.startDate
            )}..${epochToEnglishDate(
                statParams.endDate
            )}"
        return apiService.getIssuesByBucket(query).await().totalCount.toString()
    }

    private suspend fun allCommits(statParams: StatParams): String {
        return apiService.getCommitsByBucket(
            statParams.bucketName, epochToEnglishDate(
                statParams.startDate
            ), epochToEnglishDate(
                statParams.endDate
            )
        ).await().size.toString()
    }

    private fun defaultQueryIssues(bucketName: String): String {
        return "repo:${bucketName}"
    }

    //todo: Move it in utils
    private fun epochToEnglishDate(epoch: Long): String {
        // todo: Inject DateFormat and calendar via Hilt
        val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd")
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = epoch
        return formatter.format(calendar.time)
    }


}