package com.example.aircalltest.data.repository.bucket

import androidx.paging.PagingSource
import com.example.aircalltest.data.toDomain
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.framework.retrofit.GithubService

class BucketPagingSource(private val apiService: GithubService) : PagingSource<Int, Bucket>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Bucket> {

        val pagePosition = params.key ?: STARTING_PAGE_INDEX

        return try {

            val buckets = apiService.getBuckets(page = pagePosition).buckets.toDomain()
            val prevKey = if (pagePosition == STARTING_PAGE_INDEX) null else pagePosition - 1
            val nextKey = if (buckets.isEmpty()) null else pagePosition + 1

            LoadResult.Page(buckets, prevKey, nextKey)
        } catch (exception: Exception) {
            LoadResult.Error(exception)
        }
    }

    companion object {
        private const val STARTING_PAGE_INDEX = 1
        const val PAGE_SIZE = 10
    }
}