package com.example.aircalltest.data.entities.statistics.commit


import com.google.gson.annotations.SerializedName

data class AuthorXDto(
    @SerializedName("date")
    val date: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("name")
    val name: String
)