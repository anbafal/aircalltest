package com.example.aircalltest.data.entities.statistics.commit


import com.google.gson.annotations.SerializedName

data class TreeDto(
    @SerializedName("sha")
    val sha: String,
    @SerializedName("url")
    val url: String
)