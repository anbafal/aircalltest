package com.example.aircalltest.data.entities.statistics.commit


import com.google.gson.annotations.SerializedName

data class CommitDtoItem(
    @SerializedName("author")
    val author: AuthorDto,
    @SerializedName("comments_url")
    val commentsUrl: String,
    @SerializedName("commit")
    val commit: CommitDto,
    @SerializedName("committer")
    val committer: CommitterXDto,
    @SerializedName("html_url")
    val htmlUrl: String,
    @SerializedName("node_id")
    val nodeId: String,
    @SerializedName("parents")
    val parents: List<Parent>,
    @SerializedName("sha")
    val sha: String,
    @SerializedName("url")
    val url: String
)