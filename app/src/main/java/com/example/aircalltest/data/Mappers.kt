package com.example.aircalltest.data

import com.example.aircalltest.data.entities.bucket.BucketDto
import com.example.aircalltest.data.entities.login.UserDto
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.framework.room.bean.BucketBean
import com.example.aircalltest.framework.room.bean.UserBean

fun UserDto.toBean() = UserBean(login = login, avatar = avatarUrl)

//todo: wILL BE replace by dto->to be an when get from network and database
fun BucketDto.toDomain() = Bucket(
    id.toString(),
    name = fullName,
    owner = owner.login,
    watchers = watchersCount.toString(),
    forks = forksCount.toString()
)

fun List<BucketDto>.toDomain() = map { it.toDomain() }

fun BucketDto.toBean() = BucketBean(
    id,
    name = fullName,
    owner = owner.login,
    watchers = watchersCount.toString(),
    forks = forksCount.toString()
)

fun List<BucketDto>.toBean() = map { it.toBean() }

fun BucketBean.toDomain() = Bucket(
    id.toString(),
    name = name,
    owner = owner,
    watchers = watchers,
    forks = forks
)

@JvmName("listBucketBeanToDomain")
fun List<BucketBean>.toDomain() = map { it.toDomain() }
