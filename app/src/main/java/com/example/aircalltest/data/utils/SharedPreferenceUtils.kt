package com.example.aircalltest.data.utils

import android.content.SharedPreferences
import com.example.aircalltest.domain.usecase.base.LoginParams
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import javax.inject.Inject

class SharedPreferenceUtils @Inject constructor(
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson
) {

    private val SAVED_TEMPORARY_LOGIN = "SAVED_TEMPORARY_LOGIN_KEY"


    // todo: Should be save crypted and delete it when success

    fun saveTemporaryLogin(loginParams: LoginParams) {
        val editor = sharedPreferences.edit()
        val loginParamsString = gson.toJson(loginParams)
        editor.putString(SAVED_TEMPORARY_LOGIN, loginParamsString)
        editor.apply()
    }

    fun getTemporaryLogin(): LoginParams {
        val default = LoginParams("", "")
        val loginParamsString =
            sharedPreferences.getString(SAVED_TEMPORARY_LOGIN, null)
                ?: return default
        return try {
            gson.fromJson(loginParamsString, LoginParams::class.java)
        } catch (e: JsonSyntaxException) {
            default
        }
    }
}