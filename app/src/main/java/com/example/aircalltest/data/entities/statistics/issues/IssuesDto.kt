package com.example.aircalltest.data.entities.statistics.issues


import com.google.gson.annotations.SerializedName

data class IssuesDto(
    @SerializedName("incomplete_results")
    val incompleteResults: Boolean,
    @SerializedName("items")
    val items: List<ItemDto>,
    @SerializedName("total_count")
    val totalCount: Int
)