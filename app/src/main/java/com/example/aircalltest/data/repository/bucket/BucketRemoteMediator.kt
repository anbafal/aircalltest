package com.example.aircalltest.data.repository.bucket


import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.aircalltest.data.toBean
import com.example.aircalltest.framework.retrofit.GithubService
import com.example.aircalltest.framework.room.AppDatabase
import com.example.aircalltest.framework.room.bean.BucketBean
import com.example.aircalltest.framework.room.bean.RemoteKeysBean
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException
import javax.inject.Inject

//todo: Implement it if have a time. Pay attention you have to manage loading from database by default (if network failed) until fixing bug in next release

@OptIn(ExperimentalPagingApi::class)
class BucketRemoteMediator @Inject constructor(
    private val apiService: GithubService,
    private val database: AppDatabase
) : RemoteMediator<Int, BucketBean>() {

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, BucketBean>
    ): MediatorResult {
        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }

            LoadType.APPEND, LoadType.PREPEND -> {
                if (loadType == LoadType.PREPEND)
                    return MediatorResult.Success(endOfPaginationReached = true)
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys == null || remoteKeys.nextKey == null) {
                    throw InvalidObjectException("Remote key should not be null for $loadType")
                }
                remoteKeys.nextKey
            }

        }

        try {
            val bucketsDto = apiService.getBuckets(page = page).buckets
            val endOfPaginationReached = bucketsDto.isEmpty()

            database.withTransaction {
                // clear all tables in the database
                if (loadType == LoadType.REFRESH) {
                    database.remoteKeysDao().deleteRemoteKeys()
                    database.bucketDao().deleteBuckets()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = bucketsDto.map {
                    RemoteKeysBean(bucketId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                database.remoteKeysDao().insertAll(keys)
                database.bucketDao().insertAll(bucketsDto.toBean())
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, BucketBean>): RemoteKeysBean? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull() { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { bucket ->
                // Get the remote keys of the last item retrieved
                database.remoteKeysDao().remoteKeysBucketId(bucket.id)
            }
    }


    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, BucketBean>
    ): RemoteKeysBean? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { bucketId ->
                database.remoteKeysDao().remoteKeysBucketId(bucketId)
            }
        }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, BucketBean>): RemoteKeysBean? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { bucket ->
                // Get the remote keys of the first items retrieved
                database.remoteKeysDao().remoteKeysBucketId(bucket.id)
            }
    }

    companion object {
        private const val STARTING_PAGE_INDEX = 1
        const val PAGE_SIZE = 50
    }
}