package com.example.aircalltest.data.entities.bucket


import com.google.gson.annotations.SerializedName

data class PermissionsDto(
    @SerializedName("admin")
    val admin: Boolean,
    @SerializedName("pull")
    val pull: Boolean,
    @SerializedName("push")
    val push: Boolean
)