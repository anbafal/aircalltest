package com.example.aircalltest.data.entities.login


import com.google.gson.annotations.SerializedName

data class PlanDto(
    @SerializedName("collaborators")
    val collaborators: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("private_repos")
    val privateRepos: Int,
    @SerializedName("space")
    val space: Int
)