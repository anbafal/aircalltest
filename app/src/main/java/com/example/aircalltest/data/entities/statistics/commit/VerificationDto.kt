package com.example.aircalltest.data.entities.statistics.commit


import com.google.gson.annotations.SerializedName

data class VerificationDto(
    @SerializedName("payload")
    val payload: Any,
    @SerializedName("reason")
    val reason: String,
    @SerializedName("signature")
    val signature: Any,
    @SerializedName("verified")
    val verified: Boolean
)