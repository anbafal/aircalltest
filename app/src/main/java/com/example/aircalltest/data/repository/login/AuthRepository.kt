package com.example.aircalltest.data.repository.login

import com.example.aircalltest.data.toBean
import com.example.aircalltest.domain.User
import com.example.aircalltest.domain.repository.IAuthRepository
import com.example.aircalltest.framework.retrofit.GithubService
import com.example.aircalltest.framework.room.dao.UserDao
import com.example.aircalltest.framework.room.toDomain
import javax.inject.Inject

class AuthRepository @Inject constructor(
    private val apiService: GithubService,
    private val userDao: UserDao
) : IAuthRepository {
    override suspend fun login(): User {
        val user = apiService.loginAsync().await()
        userDao.insert(user.toBean())
        return userDao.loadUser()?.toDomain()!!
    }

    override suspend fun isAuthenticated(): Boolean {
        val user = userDao.loadUser()
        return user != null && user.login != ""
    }
}