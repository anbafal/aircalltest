package com.example.aircalltest.data.entities.statistics.commit


import com.google.gson.annotations.SerializedName

data class CommitDto(
    @SerializedName("author")
    val author: AuthorXDto,
    @SerializedName("comment_count")
    val commentCount: Int,
    @SerializedName("committer")
    val committer: CommitterDto,
    @SerializedName("message")
    val message: String,
    @SerializedName("tree")
    val tree: TreeDto,
    @SerializedName("url")
    val url: String,
    @SerializedName("verification")
    val verification: VerificationDto
)