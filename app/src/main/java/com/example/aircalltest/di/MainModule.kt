package com.example.aircalltest.di

import android.content.Context
import android.content.SharedPreferences
import com.example.aircalltest.data.utils.SharedPreferenceUtils
import com.example.aircalltest.domain.usecase.login.GetLoginUseCase
import com.example.aircalltest.domain.utils.DefaultDispatcherProvider
import com.example.aircalltest.domain.utils.DispatcherProvider
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object MainModule {


    @Provides
    @Singleton
    fun providesGetLoginUseCase(sharedPreferenceUtils: SharedPreferenceUtils): GetLoginUseCase {
        return GetLoginUseCase(sharedPreferenceUtils)
    }

    @Provides
    @Singleton
    fun providesSharedPreferencesUtils(
        sharedPreferences: SharedPreferences,
        gson: Gson
    ): SharedPreferenceUtils {
        return SharedPreferenceUtils(sharedPreferences, gson)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences {
        val PREF_NAME = "SHARED_PREF"
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun providesDispatcher(): DispatcherProvider {
        return DefaultDispatcherProvider()
    }
}