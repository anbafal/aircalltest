package com.example.aircalltest.di

import android.content.Context
import androidx.room.Room
import com.example.aircalltest.data.repository.bucket.BucketRepository
import com.example.aircalltest.data.repository.login.AuthRepository
import com.example.aircalltest.domain.repository.IAuthRepository
import com.example.aircalltest.domain.repository.IBucketRepository
import com.example.aircalltest.domain.utils.DispatcherProvider
import com.example.aircalltest.framework.retrofit.GithubService
import com.example.aircalltest.framework.room.AppDatabase
import com.example.aircalltest.framework.room.RoomConstants
import com.example.aircalltest.framework.room.dao.BucketDao
import com.example.aircalltest.framework.room.dao.RemoteKeyDao
import com.example.aircalltest.framework.room.dao.UserDao

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RoomModule {

    @Provides
    @Singleton
    fun providesAuthRepository(apiService: GithubService, userDao: UserDao): IAuthRepository {
        return AuthRepository(
            apiService,
            userDao
        )
    }

    @Provides
    @Singleton
    fun providesBucketRepository(
        apiService: GithubService,
        dispatcher: DispatcherProvider
    ): IBucketRepository {
        return BucketRepository(apiService, dispatcher)
    }

    @Provides
    @Singleton
    fun providesRoomDatabase(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, RoomConstants.DATABASE_NAME)
            .build()
    }

    @Provides
    @Singleton
    fun providesUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    @Singleton
    fun providesBucketDao(database: AppDatabase): BucketDao {
        return database.bucketDao()
    }

    @Provides
    @Singleton
    fun providesRemoteKeysDao(database: AppDatabase): RemoteKeyDao {
        return database.remoteKeysDao()
    }


}