package com.example.aircalltest


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.aircalltest.databinding.SingleActivityBinding
import com.example.aircalltest.presentation.features.loader.IFragmentLoaderListener
import com.example.aircalltest.presentation.features.loader.LoaderUI
import com.example.aircalltest.presentation.utils.AppBarListener
import com.example.aircalltest.presentation.utils.visibleWhen
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SingleActivity : AppCompatActivity(),
    AppBarListener, IFragmentLoaderListener {


    private lateinit var appBarConfiguration: AppBarConfiguration

    private lateinit var navController: NavController

    private lateinit var binding: SingleActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = SingleActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        // exclude fragment that we don't want that arrow for back is displayed
        //todo: If app grows do it dynamically
        appBarConfiguration =
            AppBarConfiguration(setOf(R.id.splashFragment, R.id.loginFragment, R.id.repoFragment))

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController
        navController.apply {
            NavigationUI.setupActionBarWithNavController(
                this@SingleActivity,
                this,
                appBarConfiguration
            )
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(
            navController,
            appBarConfiguration
        ) || super.onSupportNavigateUp()
    }

    override fun displayAppBar(shouldDisplay: Boolean) {
        binding.appbar.visibleWhen(shouldDisplay)
    }

    override fun showLoader(loaderUI: LoaderUI) {
        with(binding) {
            coordinatorLayout.alpha = loaderUI.alphaScreen
            contentMain.progressBar.visibility = loaderUI.visibilityProgressBar
        }
        if (loaderUI.shouldDisabledUI()) {
            window.addFlags(LoaderUI.FLAG_ACTIVITY_DISABLED)
        } else {
            window.clearFlags(LoaderUI.FLAG_ACTIVITY_DISABLED)
        }
    }


}