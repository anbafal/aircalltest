package com.example.aircalltest.framework.retrofit

import com.example.aircalltest.data.entities.bucket.ResponseDto
import com.example.aircalltest.data.entities.login.UserDto
import com.example.aircalltest.data.entities.statistics.commit.ResponseCommitDto
import com.example.aircalltest.data.entities.statistics.issues.IssuesDto
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GithubService {


    @GET(RetrofitConstants.LOGIN_LINK)
    fun loginAsync(): Deferred<UserDto>


    //todo: Dynamically set the language getting from UI
    @GET(RetrofitConstants.BUCKET_LIST_LINK)
    suspend fun getBuckets(
        @Query("q") language: String = "language:Kotlin",
        @Query("page") page: Int
    ): ResponseDto

    @GET(RetrofitConstants.ISSUES_LIST_LINK)
    fun getIssuesByBucket(@Query("q", encoded = true) query: String): Deferred<IssuesDto>

    @GET(RetrofitConstants.COMMIT_LIST_LINK)
    fun getCommitsByBucket(
        @Path("bucketName", encoded = true) bucketName: String,
        @Query("since") since: String,
        @Query("until") until: String
    ): Deferred<ResponseCommitDto>


}