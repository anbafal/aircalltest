package com.example.aircalltest.framework.room

object RoomConstants {
    const val DATABASE_NAME = "AppDatabase"
    const val TABLE_USER = "users"
    const val TABLE_BUCKET = "buckets"
    const val TABLE_REMOTE_KEY = "remote_keys"
}