package com.example.aircalltest.framework.room.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.aircalltest.framework.room.RoomConstants

@Entity(tableName = RoomConstants.TABLE_USER)
class UserBean(@PrimaryKey(autoGenerate = false) val login: String, val avatar: String)