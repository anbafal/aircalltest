package com.example.aircalltest.framework.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.aircalltest.framework.room.bean.UserBean


@Dao
interface UserDao : BaseDao<UserBean> {

    @Query("SELECT * FROM users limit 1")
    suspend fun loadUser(): UserBean?
}