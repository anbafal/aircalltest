package com.example.aircalltest.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.aircalltest.framework.room.bean.BucketBean
import com.example.aircalltest.framework.room.bean.RemoteKeysBean
import com.example.aircalltest.framework.room.bean.UserBean
import com.example.aircalltest.framework.room.dao.BucketDao
import com.example.aircalltest.framework.room.dao.RemoteKeyDao
import com.example.aircalltest.framework.room.dao.UserDao


//todo: exportSchema remove before release app

@Database(
    entities = [UserBean::class, BucketBean::class, RemoteKeysBean::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
    abstract fun bucketDao(): BucketDao
    abstract fun remoteKeysDao(): RemoteKeyDao

}