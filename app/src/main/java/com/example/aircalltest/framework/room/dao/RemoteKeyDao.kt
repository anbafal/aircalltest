package com.example.aircalltest.framework.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.aircalltest.framework.room.bean.RemoteKeysBean


@Dao
interface RemoteKeyDao : BaseDao<RemoteKeysBean> {


    @Query("SELECT * FROM remote_keys WHERE bucketId = :bucketId")
    suspend fun remoteKeysBucketId(bucketId: Long): RemoteKeysBean?

    @Query("DELETE FROM remote_keys")
    suspend fun deleteRemoteKeys()
}