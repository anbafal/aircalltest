package com.example.aircalltest.framework.room.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.aircalltest.framework.room.RoomConstants

@Entity(tableName = RoomConstants.TABLE_BUCKET)
class BucketBean(
    @PrimaryKey val id: Long, val name: String, val owner: String,
    val watchers: String, val forks: String
)