package com.example.aircalltest.framework.room

import com.example.aircalltest.domain.User
import com.example.aircalltest.framework.room.bean.UserBean

fun UserBean.toDomain() = User(login = login)