package com.example.aircalltest.framework.retrofit

object RetrofitConstants {
    //todo: base link could be also be put in BuildConfig
    const val BASE_LINK = "https://api.github.com/"
    const val LOGIN_LINK = "user"
    const val AUTHORIZATION_HEADER_KEY = "Authorization"

    const val BUCKET_LIST_LINK = "search/repositories?sort=stars"

    // All Issues -->"search/issues?q=repo:android/architecture-components-samples+created:2020-06-15..2020-06-22"
    //PR -->search/issues?q=repo:android/architecture-components-samples+type:pr+created:2020-06-15..2020-06-22
    //Bug --> search/issues?q=repo:android/architecture-components-samples+label:bug+created:2016-01-01..2020-06-22
    const val ISSUES_LIST_LINK = "search/issues"

    ///repos/android/architecture-components-samples/commits?since=2020-06-15&until=2020-06-22
    const val COMMIT_LIST_LINK = "repos/{bucketName}/commits"
}