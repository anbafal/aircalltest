package com.example.aircalltest.framework.room.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import com.example.aircalltest.framework.room.bean.BucketBean


@Dao
interface BucketDao : BaseDao<BucketBean> {


    @Query("DELETE FROM BUCKETS")
    suspend fun deleteBuckets()

    @Query(
        "SELECT * FROM buckets ORDER BY watchers DESC"
    )
    fun loadBuckets(): PagingSource<Int, BucketBean>
}