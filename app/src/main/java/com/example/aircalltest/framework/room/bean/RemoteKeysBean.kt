package com.example.aircalltest.framework.room.bean

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.aircalltest.framework.room.RoomConstants

@Entity(tableName = RoomConstants.TABLE_REMOTE_KEY)
data class RemoteKeysBean(
    @PrimaryKey
    val bucketId: Long,
    val prevKey: Int?,
    val nextKey: Int?
)