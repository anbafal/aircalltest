package com.example.aircalltest.framework.retrofit

import com.example.aircalltest.domain.usecase.login.GetLoginUseCase
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class BasicAuthInterceptor @Inject constructor(private val getLoginUseCase: GetLoginUseCase) :
    Interceptor {


    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request.newBuilder()
            .header(RetrofitConstants.AUTHORIZATION_HEADER_KEY, getCredentials()).build()
        return chain.proceed(authenticatedRequest)
    }

    private fun getCredentials() = Credentials.basic(
        getLoginUseCase.executeImmediate().username,
        getLoginUseCase.executeImmediate().password
    )

}