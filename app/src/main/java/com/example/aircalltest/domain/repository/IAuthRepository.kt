package com.example.aircalltest.domain.repository

import com.example.aircalltest.domain.User

interface IAuthRepository {
    suspend fun login(): User
    suspend fun isAuthenticated(): Boolean
}