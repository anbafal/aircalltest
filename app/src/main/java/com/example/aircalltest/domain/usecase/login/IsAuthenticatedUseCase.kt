package com.example.aircalltest.domain.usecase.login

import com.example.aircalltest.domain.repository.IAuthRepository
import com.example.aircalltest.domain.usecase.base.NoParams
import com.example.aircalltest.domain.usecase.base.UseCase
import javax.inject.Inject

class IsAuthenticatedUseCase @Inject constructor(private val authRepository: IAuthRepository) :
    UseCase<Boolean, NoParams>() {
    override suspend fun executeOnBackground(params: NoParams): Boolean {
        return authRepository.isAuthenticated()
    }
}