package com.example.aircalltest.domain.usecase.display_public_repo

import androidx.paging.PagingData
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.domain.repository.IBucketRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

// Up to  Paging 3 to execute in background...
class GetBucketsUseCase @Inject constructor(private val bucketRepository: IBucketRepository) {
    fun execute(): Flow<PagingData<Bucket>> {
        return bucketRepository.getTopKotlinBuckets()
    }
}


