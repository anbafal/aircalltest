package com.example.aircalltest.domain.usecase.login

import com.example.aircalltest.domain.User
import com.example.aircalltest.domain.repository.IAuthRepository
import com.example.aircalltest.domain.usecase.base.LoginParams
import com.example.aircalltest.domain.usecase.base.UseCase
import javax.inject.Inject

class LoginUseCase @Inject constructor(private val authRepository: IAuthRepository) :
    UseCase<User, LoginParams>() {
    override suspend fun executeOnBackground(params: LoginParams): User {
        return authRepository.login()
    }
}
