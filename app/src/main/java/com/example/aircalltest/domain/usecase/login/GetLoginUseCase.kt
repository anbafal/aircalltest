package com.example.aircalltest.domain.usecase.login

import com.example.aircalltest.data.utils.SharedPreferenceUtils
import com.example.aircalltest.domain.usecase.base.LoginParams
import javax.inject.Inject

class GetLoginUseCase @Inject constructor(private val sharedPreferenceUtils: SharedPreferenceUtils) {
    //todo: Having repository instead and in the repo use sharedPrefs
    fun executeImmediate(): LoginParams {
        return sharedPreferenceUtils.getTemporaryLogin()
    }
}