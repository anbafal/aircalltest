package com.example.aircalltest.domain.repository

import androidx.paging.PagingData
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.domain.Stat
import com.example.aircalltest.domain.usecase.base.StatParams
import kotlinx.coroutines.flow.Flow

interface IBucketRepository {

    fun getTopKotlinBuckets(): Flow<PagingData<Bucket>>

    suspend fun getStats(statParams: StatParams): Stat
}