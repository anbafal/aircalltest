package com.example.aircalltest.domain


data class Bucket(
    val id: String,
    val name: String,
    val owner: String,
    val watchers: String,
    val forks: String
)

data class User(val login: String)

data class Stat(
    val issues: String,
    val pulls: String,
    val bugs: String,
    val commits: String
)

/**
 * Default error model that comes from server if something goes wrong with a repository call
 */
data class ErrorModel(
    val message: String?,
    val code: Int?,
    @Transient var errorStatus: ErrorStatus
) {
    constructor(errorStatus: ErrorStatus) : this(null, null, errorStatus)
}

enum class ErrorStatus {
    /**
     * error in connecting to repository (Server or Database)
     */
    NO_CONNECTION,

    /**
     * error in getting value (Json Error, Server Error, etc)
     */
    BAD_RESPONSE,

    /**
     * Time out  error
     */
    TIMEOUT,

    /**
     * no data available in repository
     */
    EMPTY_RESPONSE,

    /**
     * an unexpected error
     */
    NOT_DEFINED,

    /**
     * bad credential
     */
    UNAUTHORIZED
}