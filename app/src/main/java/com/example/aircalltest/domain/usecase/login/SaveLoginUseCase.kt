package com.example.aircalltest.domain.usecase.login

import com.example.aircalltest.data.utils.SharedPreferenceUtils
import com.example.aircalltest.domain.usecase.base.LoginParams
import javax.inject.Inject

//todo: Having repository instead and in the repo use sharedPrefs
//todo: BaseUseCase for case like this
class SaveLoginUseCase @Inject constructor(private val sharedPreferenceUtils: SharedPreferenceUtils) {
    fun executeImmediate(loginParams: LoginParams) {
        sharedPreferenceUtils.saveTemporaryLogin(loginParams)
    }
}