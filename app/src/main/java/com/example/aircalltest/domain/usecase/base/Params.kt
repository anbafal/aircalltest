package com.example.aircalltest.domain.usecase.base


class LoginParams(val username: String, val password: String)
class StatParams(
    val bucketId: String,
    val bucketName: String,
    val startDate: Long,
    val endDate: Long
)

class NoParams