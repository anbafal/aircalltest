package com.example.aircalltest.domain.usecase.statistics

import com.example.aircalltest.domain.Stat
import com.example.aircalltest.domain.repository.IBucketRepository
import com.example.aircalltest.domain.usecase.base.StatParams
import com.example.aircalltest.domain.usecase.base.UseCase
import javax.inject.Inject

class GetStatisticsUseCase @Inject constructor(private val bucketRepository: IBucketRepository) :
    UseCase<Stat, StatParams>() {
    override suspend fun executeOnBackground(params: StatParams): Stat {
        return bucketRepository.getStats(params)
    }
}