package  com.example.aircalltest.presentation.features.repo_list.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.aircalltest.R
import com.example.aircalltest.domain.Bucket
import com.google.android.material.textview.MaterialTextView
import kotlinx.android.synthetic.main.repo_item.view.*

class BucketAdapter() :
    PagingDataAdapter<Bucket, BucketAdapter.ViewHolder>(diffUtilCallback) {

    var onItemClick: ((String, String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.repo_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name: MaterialTextView = itemView.item_bucket_name
        private val owner: MaterialTextView = itemView.item_bucket_owner
        private val nbWatchers: MaterialTextView = itemView.item_bucket_watcher
        private val nbForks: MaterialTextView = itemView.item_bucket_fork
        private lateinit var bucket: Bucket

        init {
            itemView.setOnClickListener {
                if (::bucket.isInitialized) {
                    onItemClick?.invoke(bucket.id, bucket.name)
                }
            }
        }

        fun bind(bucket: Bucket) {
            name.text = bucket.name
            owner.text = bucket.owner
            nbWatchers.text = bucket.watchers
            nbForks.text = bucket.watchers
            this.bucket = bucket
        }
    }

    companion object {
        //todo:
        private val diffUtilCallback = object : DiffUtil.ItemCallback<Bucket>() {
            override fun areItemsTheSame(oldItem: Bucket, newItem: Bucket): Boolean {
                return oldItem.name == newItem.name && oldItem.owner == newItem.owner
            }

            override fun areContentsTheSame(oldItem: Bucket, newItem: Bucket): Boolean {
                return oldItem == newItem
            }
        }
    }

}