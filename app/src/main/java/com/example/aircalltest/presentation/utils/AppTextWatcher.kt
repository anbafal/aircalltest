package com.example.aircalltest.presentation.utils

import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputEditText

class AppTextWatcher(
    private val textInputEditText: TextInputEditText,
    private val viewModel: ITextFieldChanged
) : TextWatcher {

    override fun afterTextChanged(editable: Editable) {
        viewModel.afterTextChanged(textInputEditText.id, editable.toString())
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

    }

}