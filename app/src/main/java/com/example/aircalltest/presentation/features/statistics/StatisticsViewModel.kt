package com.example.aircalltest.presentation.features.statistics

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.SavedStateHandle
import com.example.aircalltest.domain.Stat
import com.example.aircalltest.domain.usecase.base.StatParams
import com.example.aircalltest.domain.usecase.statistics.GetStatisticsUseCase
import com.example.aircalltest.presentation.base.BaseViewModel
import com.example.aircalltest.presentation.features.loader.LoaderUI
import com.example.aircalltest.presentation.utils.SingleLiveEvent

class StatisticsViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val getStatisticsUseCase: GetStatisticsUseCase
) :
    BaseViewModel(savedStateHandle) {

    private lateinit var bucketId: String
    private lateinit var bucketName: String

    private val _dataStats: SingleLiveEvent<Stat> by lazy {
        SingleLiveEvent<Stat>()
    }
    val dataStats: LiveData<Stat>
        get() = _dataStats

    fun onLaunch(bucketId: String, bucketName: String) {
        this.bucketId = bucketId
        this.bucketName = bucketName
    }

    fun onQueryStat(epochStart: Long, epochEnd: Long) {

        if (::bucketId.isInitialized && ::bucketName.isInitialized) {
            _showLoading.postValue(LoaderUI(View.VISIBLE, LoaderUI.SPLASH_SCREEN_DEFAULT_OPACITY))
            getStatisticsUseCase.execute(StatParams(bucketId, bucketName, epochStart, epochEnd)) {
                onComplete {
                    _dataStats.postValue(it)
                    _showLoading.postValue(LoaderUI(View.GONE))
                }

                onError {
                    _showLoading.postValue(LoaderUI(View.GONE))
                    _error.postValue(it)
                }

                onCancel {
                    _showLoading.postValue(LoaderUI(View.GONE))
                }
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        getStatisticsUseCase.unsubscribe()
    }
}
