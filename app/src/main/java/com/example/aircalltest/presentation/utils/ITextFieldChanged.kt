package com.example.aircalltest.presentation.utils

import androidx.annotation.IdRes

/**
 * Interface for all viewModel which need to process change on EditText
 */
interface ITextFieldChanged {

    fun afterTextChanged(@IdRes viewId: Int, content: String)

}