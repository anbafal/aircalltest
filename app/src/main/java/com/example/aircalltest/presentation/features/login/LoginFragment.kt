package com.example.aircalltest.presentation.features.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.aircalltest.databinding.LoginFragmentBinding
import com.example.aircalltest.presentation.base.BaseFragment
import com.example.aircalltest.presentation.utils.AppTextWatcher
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<LoginFragmentBinding, LoginViewModel>(), View.OnClickListener {

    private val vm: LoginViewModel by viewModels()

    override fun getViewModel(): LoginViewModel? {
        return vm
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.apply {
            loginButton.setOnClickListener(this@LoginFragment)
            getViewModel()?.let {
                loginEmailTextInputText.apply {
                    addTextChangedListener(
                        AppTextWatcher(
                            this,
                            it
                        )
                    )
                }
                loginPasswordTextInputText.apply {
                    addTextChangedListener(
                        AppTextWatcher(
                            this,
                            it
                        )
                    )
                }
            }
        }
    }

    //todo: Sending the view Id to the viewModel and let him decide what to do
    override fun onClick(v: View?) {
        getViewModel()?.onLogin()
    }
}
