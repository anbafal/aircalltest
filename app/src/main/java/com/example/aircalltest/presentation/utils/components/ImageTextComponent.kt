package com.example.aircalltest.presentation.utils.components


import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import com.example.aircalltest.R


class ImageTextComponent(
    context: Context,
    attrs: AttributeSet?
) :
    LinearLayoutCompat(context, attrs) {
    private fun initAttributes(
        context: Context,
        view: View,
        attrs: AttributeSet?
    ) {
        if (attrs != null) {
            val defaultImageViewHeight = 48f
            val defaultImageViewWidth = 48f
            val typedArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.ImageTextComponent, 0, 0
            )

            val number =
                typedArray.getString(R.styleable.ImageTextComponent_number_text)

            val numberTextView = view.findViewById<TextView>(R.id.single_component_number_textView)

            numberTextView.apply {
                text = number
                textSize = 15F
                //textColors
            }

            val title =
                typedArray.getString(R.styleable.ImageTextComponent_title_text)

            val titleTextView = view.findViewById<TextView>(R.id.single_component_textView)

            titleTextView.text = title

            val imageView =
                view.findViewById<ImageView>(R.id.single_component_imageView)
            val imageViewRes =
                typedArray.getResourceId(R.styleable.ImageTextComponent_img_src, -1)

            imageView.layoutParams.height = typedArray.getDimensionPixelSize(
                R.styleable.ImageTextComponent_img_height,
                ScreenUtils.convertDpToPixel(defaultImageViewHeight, context)
            )
            imageView.layoutParams.width = typedArray.getDimensionPixelSize(
                R.styleable.ImageTextComponent_img_width,
                ScreenUtils.convertDpToPixel(defaultImageViewWidth, context)
            )
            imageView.setImageResource(imageViewRes)
            typedArray.recycle()
        }
    }

    init {
        val view = View.inflate(
            context,
            R.layout.component_image_text_view,
            this
        )
        initAttributes(context, view, attrs)
    }
}
