package com.example.aircalltest.presentation.features.login

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.example.aircalltest.R
import com.example.aircalltest.domain.usecase.base.LoginParams
import com.example.aircalltest.domain.usecase.login.LoginUseCase
import com.example.aircalltest.domain.usecase.login.SaveLoginUseCase
import com.example.aircalltest.presentation.base.BaseViewModel
import com.example.aircalltest.presentation.features.loader.LoaderUI
import com.example.aircalltest.presentation.utils.ITextFieldChanged

class LoginViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val loginUseCase: LoginUseCase,
    private val saveLoginUseCase: SaveLoginUseCase
) :
    BaseViewModel(savedStateHandle),
    ITextFieldChanged {

    private lateinit var email: String
    private lateinit var password: String

    fun onLogin() {
        if (::email.isInitialized && ::password.isInitialized) {
            //todo: usecase to delete the last authenticated user

            _showLoading.postValue(LoaderUI(View.VISIBLE, LoaderUI.SPLASH_SCREEN_DEFAULT_OPACITY))
            saveLoginUseCase.executeImmediate(LoginParams(email, password))
            loginUseCase.execute(LoginParams(email, password)) {
                onComplete {
                    _showLoading.postValue(LoaderUI(View.GONE))
                    navigate(LoginFragmentDirections.actionLoginFragmentToRepoFragment())
                }

                onError {
                    _showLoading.postValue(LoaderUI(View.GONE))
                    _error.postValue(it)
                }

                onCancel {
                    _showLoading.postValue(LoaderUI(View.GONE))
                }
            }
        }
    }


    override fun onCleared() {
        super.onCleared()
        loginUseCase.unsubscribe()
    }

    override fun afterTextChanged(viewId: Int, content: String) {
        when (viewId) {
            R.id.login_email_text_input_text -> email = content
            R.id.login_password_text_input_text -> password = content
        }
    }
}
