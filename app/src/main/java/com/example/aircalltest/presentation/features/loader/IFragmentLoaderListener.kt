package com.example.aircalltest.presentation.features.loader

interface IFragmentLoaderListener {
    fun showLoader(loaderUI: LoaderUI)
}