package com.example.aircalltest.presentation.features.repo_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import com.example.aircalltest.databinding.RepoFragmentBinding
import com.example.aircalltest.presentation.base.BaseFragment
import com.example.aircalltest.presentation.features.repo_list.adapter.BucketAdapter
import com.example.aircalltest.presentation.features.repo_list.adapter.StateAdapter
import com.example.aircalltest.presentation.utils.visibleWhen
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RepoFragment : BaseFragment<RepoFragmentBinding, RepoViewModel>() {

    private val vm: RepoViewModel by viewModels()

    private val bucketAdapter: BucketAdapter by lazy {
        BucketAdapter()
    }

    override fun getViewModel(): RepoViewModel? {
        return vm
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RepoFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initAdapter()
        fetchBuckets()
    }

    private fun initAdapter() {

        with(binding.bucketRecyclerView) {
            //addItemDecoration(MarginItemDecoration(resources.getDimension(R.dimen.recycler_item_bottom_margin)))
            adapter = bucketAdapter.withLoadStateHeaderAndFooter(
                header = StateAdapter { bucketAdapter.retry() },
                footer = StateAdapter { bucketAdapter.retry() }
            )
        }

        bucketAdapter.addLoadStateListener { loadState ->
            loadState.refresh.let {
                binding.apply {
                    bucketErrorMsgTextView.visibleWhen(it is LoadState.Error)
                    bucketRetryButton.visibleWhen(it is LoadState.Error)
                    bucketProgressBar.visibleWhen(it is LoadState.Loading)
                    bucketRecyclerView.visibleWhen(it is LoadState.NotLoading)
                }
                if (it is LoadState.Error) {
                    binding.bucketErrorMsgTextView.text = it.error.localizedMessage
                }
            }
        }

        bucketAdapter.onItemClick = this::onItemClickHandler
    }

    private fun fetchBuckets() {
        viewLifecycleOwner.lifecycleScope.launch {
            getViewModel()?.searchBuckets()?.collectLatest {
                bucketAdapter.submitData(it)
            }
        }
    }

    private fun onItemClickHandler(clickBucketId: String, clickBucketName: String) {
        getViewModel()?.onRowClick(clickBucketId, clickBucketName)
    }
}
