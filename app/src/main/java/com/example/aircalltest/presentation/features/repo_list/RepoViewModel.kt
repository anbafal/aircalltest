package com.example.aircalltest.presentation.features.repo_list

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.aircalltest.domain.Bucket
import com.example.aircalltest.domain.usecase.display_public_repo.GetBucketsUseCase
import com.example.aircalltest.presentation.base.BaseViewModel
import kotlinx.coroutines.flow.Flow

class RepoViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val getBucketsUseCase: GetBucketsUseCase
) :
    BaseViewModel(savedStateHandle) {

    private var currentSearchResult: Flow<PagingData<Bucket>>? = null

    fun searchBuckets(): Flow<PagingData<Bucket>> {
        val lastResult = currentSearchResult
        if (lastResult != null) {
            return lastResult
        }
        val newResult = getBucketsUseCase.execute().cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }


    fun onRowClick(clickBucketId: String, clickBucketName: String) {
        navigate(
            RepoFragmentDirections.actionRepoFragmentToStatisticsFragment(
                clickBucketId,
                clickBucketName
            )
        )
    }


}
