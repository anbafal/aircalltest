package com.example.aircalltest.presentation.base


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.example.aircalltest.R
import com.example.aircalltest.presentation.features.loader.IFragmentLoaderListener
import com.example.aircalltest.presentation.features.loader.IViewModelLoader
import com.example.aircalltest.presentation.utils.AppBarListener
import com.example.aircalltest.presentation.utils.NavigationCommand
import com.example.aircalltest.presentation.utils.ObserveUtils
import com.example.aircalltest.presentation.utils.toast


abstract class BaseFragment<VDB : ViewBinding, VM : BaseViewModel?> : Fragment(),
    ObserveUtils {

    protected var _binding: VDB? = null

    // This property is only valid between onCreateView and onDestroyView.
    protected val binding get() = _binding!!

    protected open fun getViewModel(): VM? = null

    private val navController: NavController by lazy { findNavController() }

    private lateinit var appBarListener: AppBarListener

    private lateinit var fragmentLoaderListener: IFragmentLoaderListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        appBarListener = if (context is AppBarListener) {
            context
        } else {
            throw RuntimeException("Parent Activity must implement AppBarListener")
        }

        fragmentLoaderListener = if (context is IFragmentLoaderListener) {
            context
        } else {
            throw RuntimeException("Parent Activity must implement IFragmentLoaderListener")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        appBarListener.displayAppBar(displayAppBar())

        getViewModel().let {
            it?.error?.observe(Observer(this@BaseFragment::observedError))
            it?.navigationCommand?.observe(Observer(this@BaseFragment::observedNavigationCommand))
        }

        if (getViewModel() is IViewModelLoader) {
            (getViewModel() as IViewModelLoader).showLoading.observe(Observer {
                fragmentLoaderListener.showLoader(it)
            })
        }

    }

    private fun observedError(error: String) {
        error.toast(context)
    }

    private fun observedNavigationCommand(navigationCommand: NavigationCommand) {
        when (navigationCommand) {
            is NavigationCommand.To -> navController.navigate(navigationCommand.directions)
            is NavigationCommand.Back -> navController.popBackStack()
            is NavigationCommand.BackTo -> navController.navigate(navigationCommand.destinationId)
            is NavigationCommand.ToRoot -> navController.navigate(R.id.repoFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    open fun displayAppBar() = true

    override fun getObserveLifecycleOwner(): LifecycleOwner {
        return viewLifecycleOwner
    }
}