package com.example.aircalltest.presentation.utils

interface AppBarListener {
    fun displayAppBar(shouldDisplay: Boolean)
}