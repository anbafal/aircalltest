package com.example.aircalltest.presentation.features.statistics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.aircalltest.databinding.StatisticsFragmentBinding
import com.example.aircalltest.domain.Stat
import com.example.aircalltest.presentation.base.BaseFragment
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.component_image_text_view.view.*

@AndroidEntryPoint
class StatisticsFragment : BaseFragment<StatisticsFragmentBinding, StatisticsViewModel>(),
    View.OnClickListener {


    private val navigationArgs: StatisticsFragmentArgs by navArgs()
    private val vm: StatisticsViewModel by viewModels()

    override fun getViewModel(): StatisticsViewModel? {
        return vm
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = StatisticsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        with(binding.statsCalendarImageView) {
            setOnClickListener(this@StatisticsFragment)
        }

        getViewModel()?.onLaunch(navigationArgs.clickBucketId, navigationArgs.clickBucketName)
        getViewModel().let {
            it?.onLaunch(navigationArgs.clickBucketId, navigationArgs.clickBucketName)
            it?.dataStats?.observe(Observer(this@StatisticsFragment::observedDataStats))
        }
    }

    private fun observedDataStats(stat: Stat) {
        with(binding) {
            statIssueImageTextComponent.single_component_number_textView.text = stat.issues
            statPullImageTextComponent.single_component_number_textView.text = stat.pulls
            statBugImageTextComponent.single_component_number_textView.text = stat.bugs
            statCommitImageTextComponent.single_component_number_textView.text = stat.commits
        }
    }

    override fun onClick(v: View?) {
        // todo: send id to view model and let him decide what to do
        activity?.supportFragmentManager?.let {
            val builder = MaterialDatePicker.Builder.dateRangePicker()
            val picker = builder.build()
            //todo: By default display only the past year calendar and select by default one week
            picker.apply {
                show(it, this.toString())
                addOnPositiveButtonClickListener {
                    it.first?.let { epochStart ->
                        it.second?.let { epochEnd ->
                            getViewModel()?.onQueryStat(
                                epochStart,
                                epochEnd
                            )
                        }
                    }
                }
            }
        }
    }

}



