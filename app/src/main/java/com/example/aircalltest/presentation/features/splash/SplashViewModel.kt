package com.example.aircalltest.presentation.features.splash

import android.view.View
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import com.example.aircalltest.domain.usecase.base.NoParams
import com.example.aircalltest.domain.usecase.login.IsAuthenticatedUseCase
import com.example.aircalltest.presentation.base.BaseViewModel
import com.example.aircalltest.presentation.features.loader.LoaderUI

class SplashViewModel @ViewModelInject constructor(
    @Assisted savedStateHandle: SavedStateHandle,
    private val isAuthenticatedUseCase: IsAuthenticatedUseCase
) :
    BaseViewModel(savedStateHandle) {

    init {
        isAuthenticated()
    }


    //todo: if user exists going to login fragment else repofragment
    private fun isAuthenticated() {
        _showLoading.postValue(LoaderUI(View.VISIBLE, LoaderUI.SPLASH_SCREEN_DEFAULT_OPACITY))
        isAuthenticatedUseCase.execute(NoParams()) {
            onComplete(this@SplashViewModel::onCompleted)

            onError {
                _showLoading.postValue(LoaderUI(View.GONE))
            }

            onCancel {
                _showLoading.postValue(LoaderUI(View.GONE))
            }
        }
    }

    private fun onCompleted(authenticated: Boolean) {
        _showLoading.postValue(LoaderUI(View.GONE))
        if (authenticated) {
            navigate(SplashFragmentDirections.actionSplashFragmentToRepoFragment())
        } else {
            navigate(SplashFragmentDirections.actionSplashFragmentToLoginFragment())
        }
    }

    override fun onCleared() {
        super.onCleared()
        isAuthenticatedUseCase.unsubscribe()
    }
}
