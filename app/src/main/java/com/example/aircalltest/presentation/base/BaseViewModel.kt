package com.example.aircalltest.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.example.aircalltest.presentation.features.loader.IViewModelLoader
import com.example.aircalltest.presentation.features.loader.LoaderUI
import com.example.aircalltest.presentation.utils.NavigationCommand
import com.example.aircalltest.presentation.utils.SingleLiveEvent


abstract class BaseViewModel(private val savedStateHandle: SavedStateHandle) : ViewModel(),
    IViewModelLoader {

    private val _navigationCommand: SingleLiveEvent<NavigationCommand> by lazy {
        SingleLiveEvent<NavigationCommand>()
    }
    internal val navigationCommand: LiveData<NavigationCommand>
        get() = _navigationCommand

    fun navigate(directions: NavDirections) {
        _navigationCommand.postValue(NavigationCommand.To(directions))
    }

    protected val _showLoading: MutableLiveData<LoaderUI> by lazy {
        MutableLiveData<LoaderUI>()
    }
    override val showLoading: LiveData<LoaderUI>
        get() = _showLoading

    //todo: Send later ErrorModel class
    protected val _error: SingleLiveEvent<String> by lazy {
        SingleLiveEvent<String>()
    }
    val error: LiveData<String>
        get() = _error

}